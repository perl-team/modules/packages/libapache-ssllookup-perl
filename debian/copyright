Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Apache-SSLLookup
Upstream-Contact: Geoffrey Young <geoff@modperlcookbook.org>
Upstream-Name: Apache-SSLLookup

Files: *
Copyright: 2004, Geoffrey Young <geoff@modperlcookbook.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2015-2018, Christopher Hoskin <mans0954@debian.org>
License: Artistic or GPL-1+

Files: debian/patches/MP_dTHX.patch
Copyright: 2002-2011, The Apache Software Foundation
License: Apache-2.0

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
     https://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache License,
 Version 2.0 can be found in `/usr/share/common-licenses/Apache-2.0'.
